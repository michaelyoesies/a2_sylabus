// const s = "Hello, World";

// console.log(s.substring(0, 5).toUpperCase());
// console.log(s.split(", "));

// Arrays

// const fruits = ["apples", "oranges", "pears"];

// fruits[3] = "grapes";

// fruits.push("mangos");
// fruits.unshift("strawberries");

// fruits.pop();

// console.log(Array.isArray(fruits));

// console.log(fruits.indexOf("oranges"));

// console.log(fruits);
// console.log(fruits[0]);

// object

// const person = {
//   firstName: "John",
//   lastName: "Doe",
//   age: 30,
//   hobbies: ["music", "movies", "sports"],
//   address: {
//     street: "50 main st",
//     city: "Boston",
//     state: "Ma",
//   },
// };

// console.log(person.hobbies[1], person.address.city);
// const {
//   firstName,
//   lastName,
//   address: { city },
// } = person;
// console.log(firstName, city);

// person.email = "john@gmail.com";
// console.log(person);

// const todos = [
//   {
//     id: 1,
//     text: "Take out trash",
//     isCompleted: true,
//   },
//   {
//     id: 2,
//     text: "Meeting with boss",
//     isCompleted: true,
//   },
//   {
//     id: 3,
//     text: "Dentist appt",
//     isCompleted: false,
//   },
// ];

// const todoJSON = JSON.stringify(todos);
// console.log(todoJSON);

// For loop
// for (let i = 0; i < 10; i++) {
//   console.log(i);
// }

// While loop
// let i = 0;
// while (i < 10) {
//   console.log(i);
//   i++;
// }

// for (let i = 0; i < todos.length; i++) {
//   console.log(todos[i].text);
// }

// for (let todo of todos) {
//   console.log(todo);
// }

// forEach, map, filter

// todos.forEach(function (todo) {
//   console.log(todo.text);
// });

// const todoText = todos.map(function (todo) {
//   return todo.text;
// });

// console.log(todoText);

// const todoCompleted = todos
//   .filter(function (todo) {
//     return todo.isCompleted === true;
//   })
//   .map(function (todo) {
//     return todo.text;
//   });

// console.log(todoCompleted);

// const addNums = (num1, num2) => num1 + num2;
// console.log(addNums(1, 2));

//Constructor function
// function Person(firstName, lastName, dob) {
//   this.firstName = firstName;
//   this.lastName = lastName;
//   this.dob = new Date(dob);
// }

// Person.prototype.getBirthYear = function () {
//   return this.dob.getFullYear();
// };

// Person.prototype.getFullName = function () {
//   return `${this.firstName} ${this.lastName}`;
// };

//Class
// class Person {
//   constructor(firstName, lastName, dob) {
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.dob = new Date(dob);
//   }

//   getBirthYear() {
//     return this.dob.getFullYear();
//   }

//   getFullName() {
//     return `${this.firstName} ${this.lastName}`;
//   }
// }

//Instantiate object
// const person1 = new Person("John", "Doe", "4-3-1980");
// const person2 = new Person("Marry", "Smith", "3-6-1978");
// console.log(person1);
// console.log(person2.dob);

// console.log(person1.getBirthYear());
// console.log(person1.getFullName());
// console.log(person1);
